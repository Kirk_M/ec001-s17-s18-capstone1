package com.zuitt.discussion.Service;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);

    ResponseEntity createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User post);


}
